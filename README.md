# EasyNote

# Présentation 
Ceci est un projet scolaire crée en ***PHP Symphony***. Nous devions réaliser un site permettant à une université d'insérer les notes des étudiants. L'objectif de ce projet était de réaliser une maquette du site, il s'agit donc uniquement d'un prototype d'une maquette.

# Requirement
- Composer [https://getcomposer.org/]
- PHP
- Symphony [https://symfony.com/download]

# Lancer le serveur local
Ouvrez un **terminal** sur le dossier puis executer la **commande** suivante :
`php bin/console server:run`

# Affichage des différentes pages du site
**Page d'accueil** :
![](./image-readme/HomePage.png)
**Page des notes** :
![](./image-readme/NotePage.png)
**Page des corrections de note** :
![](./image-readme/CorrectionPage.png)
**Page des statistiques des professeurs** :
![](./image-readme/StatProfPage.png)
**Page des statistiques des universités** :
![](./image-readme/StatUFRPage.png)