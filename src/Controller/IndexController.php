<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
     /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('index/home.html.twig');
    }

    /**
     * @Route("/correction", name="correction")
     */
    public function correction() {
        return $this->render('index/correction.html.twig');
    }

    /**
     * @Route("/notes", name="notes")
     */
    public function notes() {
        return $this->render('index/notes.html.twig');
    }

    /**
     * @Route("/prof", name="prof")
     */
    public function prof() {
        return $this->render('index/prof.html.twig');
    }

    /**
     * @Route("/ufr", name="ufr")
     */
    public function ufr() {
        return $this->render('index/ufr.html.twig');
    }
}
